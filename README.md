
# Happy API with Rust  

## Setup the project 
### Rust

Install Rust via rustup: `curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`

### IDE

Install VSCode plugin `rust-analyszer`: https://rust-analyzer.github.io/

### Linting et formating

The Rust team maintains clippy, the official Rust linter.
You can install it with: `rustup component add clippy`

You can run clippy on your project with: `cargo clippy`

rustfmt is the official Rust formatter.
You can install it with: `rustup component add rustfmt`

You can run fmt on your project with: `cargo fmt`


**clippy and rustfmt are included in the set of default components installed by rustup**

### Security audit

The Rust Secure Code working group maintains an Advisory Database - an up-to-date collection of reported vulnerabilities for crates published on crates.io. 

You can install it with: `cargo install cargo-audit`

Once installed, run: `cargo audit`

### Git hooks and pipeline

There is always debates about fix bad pratices with git hooks or in pipelines.
In my personal opinion you should always do both:
- Using git hooks give a quicker feedback to the developer and avoid a bad commit/push
- Since client hooks can always be bypass by lazy developers you must also do the same checks in the pipeline.

#### Hooks

Just add rusty-hook as a dev dependency in your Cargo.toml file:
```toml
[dev-dependencies]
rusty-hook = "^0.11.2"
```

create a file named `rusty-hook.toml` with:
```toml
[hooks]
pre-commit = "cargo fmt -- --check && cargo -- -D warnings && cargo audit && cargo test"
post-commit = "echo '🪝🦀 commit success with hook'"

[logging]
verbose = true
```

> 📌 Note that clippy can automatically apply some lint suggestions but at the moment I write this training this is still experimental and only supported on the nightly channel ; so I encourage to fix lint issues manually

#### CI

Create a `.gitlab-ci.yml` file with:
```yml
image: "rust:latest"

default:
  before_script:
    - rustc --version 
    - cargo --version

stages:
  - test

test-code:
  stage: test
  script:
    - cargo test

lint-code:
  stage: test
  script:
    - rustup component add clippy
    - cargo clippy -- -D warnings
    
format-code:
  stage: test
  script:
    - rustup component add rustfmt
    - cargo fmt -- --check

audit-code:
  stage: test
  script:
    - cargo install cargo-audit
    - cargo audit
```

## Web Stack

We will use `actix-web` web framework.
- [actix-web’s website](https://actix.rs/)
- [actix-web’s documentation](https://docs.rs/actix-web/4.0.0-beta.3/actix_web/)
- [actix-web’s examples collection](https://github.com/actix/examples)


With `sqlx` compiled-time checked queries builder.
- [sqlx's repository](https://github.com/launchbadge/sqlx#usage)
- [sqlx's documentation](https://docs.rs/sqlx/0.5.5/sqlx/)

### Heartbeat

Let’s try to get off the ground by implementing a heartbeat endpoint: when we receive a GET request for /heartbeat we want to return a 200 OK response with no body.

#### Install actix-web

First add required dependencies in your `Cargo.toml`
```toml
[dependencies]
# We are using the latest beta release of actix-web
# that relies on tokio 1.x.x
# There is _some_ turbulence when working with betas,
# we are pinning a few other supporting packages to ensure
# compatibility.
actix-web = "=4.0.0-beta.5"
actix-http = "=3.0.0-beta.5"
actix-service = "=2.0.0-beta.5"
```

> 📌 Remember `cargo install xxx` is for installing crates with a binary globally (similar to `npm i -g xxx`)

If you prefer use command `cargo add xxx` (similar to `npm i xxx`), you must install [cargo-edit](https://github.com/killercup/cargo-edit), a community-maintained cargo extension: `cargo install cargo-edit`

Then you can install crates with: `cargo add actix-web --vers=4.0.0-beta.5`  

> ⚠️ There is an [issu with beta / dev lib version](https://github.com/RustSec/rustsec/issues/300) that will force us to deactivate `cargo audit for the rest of the training ... but you should activate it in production projects.

Create a first web app, in the `src/main.rs`:
```rust
use actix_web::{web, App, HttpRequest, HttpServer, Responder};

async fn heartbeat(req: HttpRequest) -> impl Responder {
    let name = req
        .match_info()
        .get("name")
        .unwrap_or("He Who Cannot Be Named");
    format!("I am Alive! Are you my daddy {}?", &name)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(heartbeat))
            .route("/{name}", web::get().to(heartbeat))
    })
    .bind("127.0.0.1:8000")?
    .run()
    .await
}
```

Anatomy of the application:
- `HttpServer` is ... a http server ... seriously 😁 It handles all transport level concerns
- `App` is where all your application logic lives: routing, middlewares, request handlers, etc <br/>
**App** is a struct created using the *builder pattern* `new()` which is quite common in Rust
- `route` function take 2 parameters:
  - _path_, a string, possibly templated (e.g. "/{name}") to accommodate dynamic path segments
  - _route_, an instance of the Route struct. Route combines a handler with a set of guards.

- `heartbeat` is an asynchronous function that takes an `HttpRequest` as input and returns something that implements the `Responder` trait. A type implements the Responder trait if it can be converted into a `HttpResponse`.

Then you can run the application : `cargo run` 
and tests that the endpoint is working : `curl -v http://localhost:8000/`

#### Asynchronous programming in Rust

##### Async VS OS Thread

**OS threads** are suitable for a small number of tasks, since threads come with CPU and memory overhead. Spawning and switching between threads is quite expensive as even idle threads consume system resources. A thread pool library can help mitigate some of these costs, but not all. However, threads let you reuse existing synchronous code without significant code changes—no particular programming model is required. In some operating systems, you can also change the priority of a thread, which is useful for drivers and other latency sensitive applications.

**Async** provides significantly reduced CPU and memory overhead, especially for workloads with a large amount of IO-bound tasks, such as servers and databases. All else equal, you can have orders of magnitude more tasks than OS threads, because an async runtime uses a small amount of (expensive) threads to handle a large amount of (cheap) tasks. However, async Rust results in larger binary blobs due to the state machines generated from async functions and since each executable bundles an async runtime.

##### Async Rust

Asynchronous programming in Rust is built on top of the Future trait: a future stands for a value that may not be there yet. All futures expose a poll method which has to be called to allow the future to make progress and eventually resolve to its final value. You can think of Rust’s futures as lazy: unless polled, there is no guarantee that they will execute to completion.

Rust’s standard library does not include an asynchronous runtime: you are supposed to bring one into your project as a dependency.

So **main** cannot be an asynchronous function: we need something that is in charge to `pool` the future. You are therefore expected to launch your asynchronous runtime at the top of your main function and then use it to drive your futures to completion.

actix’s runtime is built on top of an asynchronous runtime: [tokio](https://tokio.rs/)

`#[actix_web::main]` is a procedural macro that gives us the illusion of being able to define an asynchronous main while, under the hood, it just takes our main asynchronous body and writes the necessary boilerplate to make it run on top of actix’s runtime.

If you want to demistify `#[actix_web::main]` you can install `cargo install cargo-expand` and run `cargo expand`

```rust
fn main() -> std::io::Result<()> {
    actix_web::rt::System::new().block_on(async move {
        {
            HttpServer::new(|| {
                App::new()
                    .route("/", web::get().to(heartbeat))
                    .route("/{name}", web::get().to(heartbeat))
            })
            .bind("127.0.0.1:8000")?
            .run()
            .await
        }
    })
}
```

We are starting actix’s async runtime and we are using it to drive the future returned by HttpServer::run to completion.

## Project organisation

We will reorganize our projet to put all our logic in a library crate while the binary itself will be just the entry point. This is a common practice in rust projet and make application easiest to test.

- Create the lib file: `touch src/lib.rs`

- Update `cargo.toml`:
```toml
[package]
name = "happy"
version = "0.1.0"
authors = ["Thomas Haesslé <thaessle@cutii.io>"]
edition = "2018"

[lib]
path = "src/lib.rs"

[[bin]]
path = "src/main.rs"
name = "happy"

[dependencies]
# ...

```

- Edit `main.rs`:
```rust 
use happy::run;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    run().await
}
```

We will implement the initial specification for heartbeat 
> 📝 when we receive a GET request for /heartbeat we want to return a 200 OK response with no body.

- Edit `lib.rs`:
```rust
use actix_web::{web, App, HttpResponse, HttpServer, Responder};

async fn heartbeat() -> impl Responder {
    HttpResponse::Ok().finish()
}

pub async fn run() -> std::io::Result<()> {
    HttpServer::new(|| App::new().route("/heartbeat", web::get().to(heartbeat)))
        .bind("127.0.0.1:8000")?
        .run()
        .await
}
```

Test our api: `curl -v http://localhost:8000/heartbeat`

## Implementing a first integration test

While we might intentionally deploy breaking changes to our API contract, it is critical that we do not break it accidentally. 
Testing the API by interacting with it in the same exact way a user would is the most reliable way to check we don't have introduced a user-visible regression.


- Add  actix-rt: `cargo add actix-rt --dev --vers 2`
- Add reqwest: `cargo add reqwest --dev --vers 0.11`

Our specification was: 
> 📝 when we receive a GET request for /heartbeat we want to return a 200 OK response with no body.

We can implement the test in our `lib.rs`:
```rust
#[cfg(test)]
mod tests {
    use super::*;
    async fn spawn_app() -> std::io::Result<()> {
        run().await
    }

    #[actix_rt::test]
    async fn heartbeat_works() {
        spawn_app().await.expect("Failed to spawn our app.");
        let client = reqwest::Client::new();
        let response = client
            .get("http://127.0.0.1:8000/heartbeat")
            .send()
            .await
            .expect("Failed to execute request.");

        assert!(response.status().is_success());
        assert_eq!(Some(0), response.content_length());
    }
}
```

And run it with: `cargo test`

Sadly we get a:
```sh
test tests::heartbeat_works ... test tests::heartbeat_works has been running for over 60 seconds
```

In run we invoke (and await) `HttpServer::run`. `HttpServer::run` returns an instance of `Server` - when we call `.await` it starts listening on the address we specified indefinitely: it will handle incoming requests as they arrive, but it will never shutdown or “complete” on its own.
This implies that `spawn_app` never returns and our test logic never gets executed.

We need to run our application as a background task.

Refactor the `run` without awaiting (at the same time we will paremtrize the address via a TcpListener):
```rust
use std::net::TcpListener;
...
// We have no .await call, so async keyword is not needed anymore.
pub fn run(listner: TcpListener) -> Result<Server, std::io::Error> {
    let server = HttpServer::new(|| App::new().route("/heartbeat", web::get().to(heartbeat)))
        .listen(listner)?
        .run();
    Ok(server)
}
...
```

We need to adjust our `main.rs`:
```rust
use happy::run;
use std::net::TcpListener;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let listener = TcpListener::bind("127.0.0.1:8000").expect("Failed to bind random port");

    run(listener)?.await
}
```

- Add tokio: `cargo add tokio --dev --vers 1`

Now we can adapt our test:
```rust
#[cfg(test)]
mod tests {
    use super::*;

    fn spawn_app() -> String {
        // Port 0 is special-cased at the OS level: 
        // trying to bind port 0 will trigger an OS scan for an available port 
        // which will then be bound to the application
        // this ensure to have an available port when testing in CI
        let listener = TcpListener::bind("127.0.0.1:0").expect("Failed to bind random port");
        let port = listener.local_addr().unwrap().port();
        let app = run(listener).expect("Failed to bind address");
        // We will need to know the port the OS gives us
        let _ = tokio::spawn(app);
        // We will need to know the port the OS gives us
        format!("http://127.0.0.1:{}", port)
    }

    #[actix_rt::test]
    async fn heartbeat_works() {
        let address = spawn_app();
        let client = reqwest::Client::new();
        let response = client
            .get(&format!("{}/heartbeat", &address))
            .send()
            .await
            .expect("Failed to execute request.");

        assert!(response.status().is_success());
        assert_eq!(Some(0), response.content_length());
    }
}
```

We can run our test again: `cargo test` ! It passed 💪

Now we can focus on a more interesting domain in the [next section](./PART2.md)